#pragma once

/* Enable to use fmt's default way of pringting color even if you are on windows 
 * (This option won't work of the default CMD. You need to have a more modern terminal 
 * for colors to be printed correctly). */
//#define WIN_MODERN_TERMINAL
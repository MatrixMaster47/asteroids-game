#pragma once

#if defined(_WIN32) || defined(_WIN64)
	#if defined(ENGINE)
		#define DLL __declspec(dllexport)
	#else
		#define DLL __declspec(dllimport)
	#endif
#else defined(__linux__)
	#define DLL
#endif

#define VERIFY_OR_THROW(case, message) if(!case) { throw std::runtime_error(message); }
#define VERIFY_OR_EXIT(case, message) if(!case) { std::cout << message << std::endl; std::exit(1); }
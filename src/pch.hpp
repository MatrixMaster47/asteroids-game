#pragma once

#include <iostream>
#include <algorithm>
#include <memory>
#include <functional>

//Data structures
#include <cstdint>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>

//Engine files
#include <core/Core.hpp>

#ifdef _WIN32
	#include <Windows.h>
#endif
#include "pch.hpp"
#include "Log.hpp"

namespace Engine {
	LogLevel Log::SetLevel(LogLevel level) {
		return m_Level = level;
	}

	Log::Log(LogHandler& handler, const std::string& name, const std::string& format, LogLevel initial_level)
		: m_Level(initial_level), m_Name(name), m_Format(format) {
		m_Handlers.push_back(&handler);
	}

	Log::~Log() {}

	/*void Logger::Init() {
		StdOutHandler sh;
		OfstreamHandler oh("Internal.log");
		m_Logger = std::make_shared<Log>(sh,"Internal");
		m_Logger->AddLogHandler(oh);
	}*/
}
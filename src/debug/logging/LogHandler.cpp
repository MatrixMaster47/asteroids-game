#include "pch.hpp"
#include "LogHandler.hpp"

namespace Engine {
	//Windows CMD doesn't support default fmt colors
	#if (defined(_WIN32) || defined(_WIN64)) && !defined(WIN_MODERN_TERMINAL)
		#define STDOUTHANDLER_HANDLE_BODY SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), GetStyleFromLevel(level)); fmt::print(message); 
	#else
		#define STDOUTHANDLER_HANDLE_BODY fmt::print(GetStyleFromLevel(level), message);
	#endif
	void StdOutHandler::Handle(LogLevel level, const std::string& message) {
		STDOUTHANDLER_HANDLE_BODY
	}

	CONSOLE_STYLE_TYPE StdOutHandler::GetStyleFromLevel(LogLevel level) const {
		switch(level) {
		case LogLevel::Trace:
			return CONSOLE_COLOR_TRACE;
		case LogLevel::Warn:
			return CONSOLE_COLOR_WARN;
		case LogLevel::Error:
			return CONSOLE_COLOR_ERROR;
		case LogLevel::Critical:
			return CONSOLE_COLOR_CRITICAL;
		}
	}


	void OfstreamHandler::Handle(LogLevel level, const std::string& message) {
		m_Stream << message;
	}

	OfstreamHandler::OfstreamHandler(std::string file_name)
		: m_FileName(file_name) {
		m_Stream.open(m_FileName, std::ios::out | std::ios::trunc);
		VERIFY_OR_EXIT(m_Stream.is_open(), "OfstreamHandler could not be initialized, file could not be opened.")
	}

	Engine::OfstreamHandler::~OfstreamHandler() {
		m_Stream.close();
	}
} 
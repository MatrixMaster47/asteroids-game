#pragma once

#define FMT_HEADER_ONLY

#include <chrono>
#include <ctime>

#include <fmt/include/fmt/core.h>
#include <fmt/include/fmt/color.h>
#include <fmt/include/fmt/chrono.h>

#include <debug/logging/LogHandler.hpp>

using namespace fmt::literals;

namespace Engine {
	/* This is defined in LogHandler.hpp, because i need it over there and
	* defining it here would result in recursive inclusion
	enum class DLL LogLevel : uint8_t {
		Trace    = 3,
		Warn     = 2,
		Error    = 1,
		Critical = 0
	};*/

	class DLL Log {
	public:
		LogLevel SetLevel(LogLevel level);
		void AddLogHandler(LogHandler& handler) { m_Handlers.push_back(&handler); }

		template<typename T>
		std::string LogMsg(LogLevel level, const T& message) const {
			std::string MessageFormatted = fmt::format(m_Format, "t"_a = fmt::format("{:%H:%M:%S}", std::chrono::system_clock::now()), "n"_a = m_Name, "l"_a = GetNameFromLevel[(int)level], "m"_a = message);
			if(m_Level >= level) {
				for(LogHandler* handler : m_Handlers) {
					handler->Handle(level, MessageFormatted);
				}
			}
			return MessageFormatted;
		}
		template<typename T, typename... Args>
		std::string LogMsg(LogLevel level, const T& message, Args&... args) const {
			std::string MessageFormatted = fmt::format(m_Format, "t"_a = fmt::format("{:%H:%M:%S}", std::chrono::system_clock::now()), "n"_a = m_Name, "l"_a = GetNameFromLevel[(int)level], "m"_a = fmt::format(message, args));
			if(m_Level >= level) {
				for(LogHandler* handler : m_Handlers) {
					handler->Handle(level, MessageFormatted);
				}
			}
			return MessageFormatted;
		}

		Log(LogHandler& handler, const std::string& name = "Logger", const std::string & = "[{t}] {n}/{l}: {m}", LogLevel initial_level = LogLevel::Trace);
		~Log();
	private:
		const char* GetNameFromLevel[4]{
			"Crit",
			"Err",
			"Warn",
			"Trace"
		};

		LogLevel m_Level;
		std::string m_Format;
		std::string m_Name;
		std::vector<LogHandler*> m_Handlers;
	};

	/*struct Logger {
		static std::shared_ptr<Log> Get() { return m_Logger; }
		static void Init();
	private:
		static std::shared_ptr<Log> m_Logger;
	};*/
}
#pragma once

#define FMT_HEADER_ONLY

#include <fstream>

#include <fmt/include/fmt/core.h>
#include <fmt/include/fmt/color.h>

#include "Colors.hpp"
#include <core/Config.hpp>

namespace Engine {
	enum class DLL LogLevel : uint8_t {
		Trace = 3,
		Warn = 2,
		Error = 1,
		Critical = 0
	};

	class DLL LogHandler {
	public:
		virtual void Handle(LogLevel level, const std::string& message) = 0;
	};

	class DLL StdOutHandler : public LogHandler {
	public:
		void Handle(LogLevel level, const std::string& message) override;
	private:
		CONSOLE_STYLE_TYPE GetStyleFromLevel(LogLevel level) const;
	};
	class DLL OfstreamHandler : public LogHandler {
	public:
		void Handle(LogLevel level, const std::string& message) override;

		OfstreamHandler(std::string file_name);
		~OfstreamHandler();
	private:
		std::string m_FileName;
		std::ofstream m_Stream;
	};
}

#pragma once

#include <core/Config.hpp>

#if (defined(_WIN32) || defined(_WIN64)) && !defined(WIN_MODERN_TERMINAL)
	#include <Windows.h>
#endif

#include <fmt/include/fmt/color.h>

#if (defined(_WIN32) || defined(_WIN64)) && !defined(WIN_MODERN_TERMINAL)
	#define CONSOLE_STYLE_TYPE  WORD

	#define CONSOLE_COLOR_TRACE    FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY
	#define CONSOLE_COLOR_WARN     FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY
	#define CONSOLE_COLOR_ERROR    FOREGROUND_RED | FOREGROUND_INTENSITY
	#define CONSOLE_COLOR_CRITICAL FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_INTENSITY
#else
	#define CONSOLE_STYLE_TYPE fmt::text_style

	#define CONSOLE_COLOR_TRACE    fmt::fg(fmt::color::white)
	#define CONSOLE_COLOR_WARN     fmt::fg(fmt::color::yellow)
	#define CONSOLE_COLOR_ERROR    fmt::fg(fmt::color::red) | fmt::emphasis::bold
	#define CONSOLE_COLOR_CRITICAL fmt::fg(fmt::color::white) | fmt::bg(fmt::color::red)
#endif
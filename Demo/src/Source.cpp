//#include "pch.hpp"
#include <include/Engine/Engine.hpp>
//#include <src/Entrypoint.hpp>
//#include <iostream>
//#include <vector>
//#include <cstdint>

//int main() {
//	Engine::OfstreamHandler handler("test.log");
//	Engine::Log logger(handler, "Internal");
//	logger.LogMsg(Engine::LogLevel::Warn, "Hope this works");
//	std::cin.get();
//}
namespace Engine {

	void Construct() {
		OfstreamHandler handler("test.log");
		Log logger(handler, "Internal");
		logger.LogMsg(LogLevel::Warn, "Hope this works");
		std::cin.get();
	}
}